# Clinvar Service

This microservice returns information about variants. It currently uses a VCF file from [ClinVar](https://www.ncbi.nlm.nih.gov/clinvar/) found [here](https://ftp.ncbi.nlm.nih.gov/pub/clinvar/vcf_GRCh38/). 
It can be used to determine the clinical significance of a variant, as well as getting other clinical information.

It is a very early version (WIP).

More information about ClinVar can be found [here](https://www.ncbi.nlm.nih.gov/clinvar/intro/).

Parsing is done using Java (Quarkus project) and [HTSlib](https://github.com/samtools/htslib).

## Requirements <a name="requirements"></a>

If a docker-image is already provided, only [Docker](https://docs.docker.com/get-docker/) and docker-compose need to be installed. 

Otherwise [Maven](https://maven.apache.org/users/index.html) needs to be installed in addition to that.

## General Setup

1. Clone the git project
2. Install and set up the [Requirements](#requirements)

### If the docker image still needs to be build:

1. Run `mvnw package`, for example with:

```
./mvnw clean package
```

2. Create the docker image(or `tag` a container as `clinvarservice:latest`):

```
docker build -f src/main/docker/Dockerfile.jvm -t clinvarservice .
```

### With working docker images:

3. Run `docker-compose up`

To run the container manually, copy the file clinvar.vcf.gz in your bind mount directory (here: /data/path/clinvar) and run

```
docker run -d -p 8092:8092 -v /data/path/clinvar/:/mnt --name clinvarservice clinvarservice
```

4. Query the program with the wanted variants (see below)


#### Download and change the path of your source file

- The source file can be downloaded with the `downloadNewestReleaseClinvar.sh` script. It is then called `clinvar.vcf.gz`
- The location of this file probably needs to be adjusted in `docker-compose.yml`

## Querying the service

The service accepts variants in the format `chrN:positionREF>VAR`.
N can be any number between 1 and 22, X or Y.

It will return some JSON in a preliminary format.

## Endpoints

Currently, there are three endpoints:

- `clinvar/v1/simple?genompos={variant}`: Delivers simplified output only containing the **clinical significance** and metadata of the variant.

- `clinvar/v1/{genome}/full?genompos={variant}`: Delivers full JSON output of information provided by clinvar for that variant.

- `clinvar/v1/info/`: Shows some basic information regarding the tool.

### Swagger UI

`clinvar/v1/doc/`


#### Example call (the service is currently running on port 8092):

```
curl -X GET "http://localhost:8092/clinvar/v1/hg38/full?genompos=chr11:126275389C>T" | jq
```

### Possible Errors

- make sure to remove or refresh the `.tbi` file if a new one is downloaded. Else there will be an error message while handling tabix output

- if the `.tbi` file is not generated automatically, there may be a problem with permissions of your `clinvar.vcf.gz`

