from flask import request
from flask_cors import CORS
from flask import Flask
from flask_swagger_ui import get_swaggerui_blueprint
import conf.read_config as conf_reader
import clinvar_adapter.data_parser

DEBUG = True
SERVICE_NAME="clinvar"
VERSION="v1"

app = Flask(__name__)
app.config.from_object(__name__)

CORS(app, resources={r'/*': { 'origins': '*' }})
SWAGGER_URL = f'/{SERVICE_NAME}/{VERSION}/docs'
API_URL = '/static/config.json'

swaggerui_blueprint = get_swaggerui_blueprint(
    SWAGGER_URL,  # Swagger UI static files will be mapped to '{SWAGGER_URL}/dist/'
    API_URL,
    config={  # Swagger UI config overrides
        'app_name': "ClinVar-Adapter"
    },
)

# definitions
SITE = {
        'logo': 'FLASK-VUE',
        'version': '0.0.1'
}


@app.route(f'/{SERVICE_NAME}/{VERSION}/<genome_version>/full', methods=['GET'])
def get_score(genome_version=None):
    variant = request.args.get("genompos")
    variants= variant.split(",")
    response = clinvar_adapter.data_parser.get_clinvar_entry(variants, genome_version)
    return response


@app.route(f'/{SERVICE_NAME}/{VERSION}/info')
def info_endpoint():
    info_data = {}
    info_data["title"] = "ClinVar Adapter"
    info_data["database_version"] = conf_reader.__VERSION__
    info_data["database_website"] = "https://ftp.ncbi.nlm.nih.gov/pub/clinvar/"
    return info_data

if __name__ == '__main__':
    app.register_blueprint(swaggerui_blueprint)
    app.run(host='0.0.0.0', debug=True, port=conf_reader.__PORT__)
