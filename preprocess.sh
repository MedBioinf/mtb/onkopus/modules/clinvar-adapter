#!/bin/bash

NCBI_URL="https://ftp.ncbi.nlm.nih.gov/pub/clinvar/"
NCBI_URL_HG38=${NCBI_URL}"vcf_GRCh38/"
NCBI_URL_HG37=${NCBI_URL}"vcf_GRCh37/"
DB_FILENAME_HG38="clinvar_${VERSION}.vcf.gz"
DB_FILENAME_HG37="clinvar_${VERSION}.vcf.gz"

# Check if database files are present, download them if they cannot be found
if [ ! -f "${DATA_PATH}/grch38/${DB_FILENAME_HG38}" ]
then
  echo "Could not find database files. Starting file download..."
  cd "${DATA_PATH}"

  mkdir -v grch37
  mkdir -v grch38

  wget -v ${NCBI_URL_HG38}${DB_FILENAME_HG38} -P "${DATA_PATH}"
  wget -v ${NCBI_URL_HG38}${DB_FILENAME_HG38}.tbi -P "${DATA_PATH}"

  mv -v ${DB_FILENAME_HG38} ./grch38/
  mv -v ${DB_FILENAME_HG38}.tbi ./grch38/

  wget -v ${NCBI_URL_HG37}${DB_FILENAME_HG37} -P "${DATA_PATH}"
  wget -v ${NCBI_URL_HG37}${DB_FILENAME_HG37}.tbi -P "${DATA_PATH}"

  mv -v ${DB_FILENAME_HG37} ./grch37/
  mv -v ${DB_FILENAME_HG37}.tbi ./grch37/

  # update symlink
  cd grch38
  ln -vfns ./${DB_FILENAME_HG38} ./clinvar_hg38.vcf.gz
  ln -vfns ./${DB_FILENAME_HG38}.tbi ./clinvar_hg38.vcf.gz.tbi

  cd ../grch37
  ln -vfns ./${DB_FILENAME_HG37} ./clinvar_hg37.vcf.gz
  ln -vfns ./${DB_FILENAME_HG37}.tbi ./clinvar_hg37.vcf.gz.tbi
else
  echo "Database files found in ${OUT_DIR}"
fi

/opt/venv/bin/python3 /app/app.py



