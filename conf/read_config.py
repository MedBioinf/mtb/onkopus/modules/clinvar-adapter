import os, configparser

config = configparser.ConfigParser()
config.read(os.path.join(os.path.dirname(__file__), '', 'config.ini'))

if "MODULE_SERVER" in os.environ:
    __MODULE_SERVER__ = os.getenv("MODULE_SERVER")
else:
    __MODULE_SERVER__ = config['DEFAULT']['MODULE_SERVER']

if "DATA_PATH" in os.environ:
    __DATA_PATH__ = os.getenv("DATA_PATH")
else:
    __DATA_PATH__ = config['CLINVAR']['DATA_PATH']

if "PORT" in os.environ:
    __PORT__ = os.getenv("PORT")
else:
    __PORT__ = config['CLINVAR']['CLINVAR_ADAPTER_PORT']

if "VERSION" in os.environ:
    __VERSION__ = os.getenv("VERSION")
else:
    __VERSION__ = config['CLINVAR']['VERSION']

if "DATABASE_FILE_HG38" in os.environ:
    __DATABASE_FILE_HG38__ = os.getenv("DATABASE_FILE_HG38")
else:
    __DATABASE_FILE_HG38__ = "clinvar_" + config['CLINVAR']['VERSION'] + ".vcf.gz"

if "DATABASE_FILE_HG19" in os.environ:
    __DATABASE_FILE_HG19__ = os.getenv("DATABASE_FILE_HG19")
else:
    __DATABASE_FILE_HG19__ = "clinvar_" + config['CLINVAR']['VERSION'] + ".vcf.gz"

if "LOG_PATH" in os.environ:
    __LOG_PATH__ = os.getenv("LOG_PATH")
else:
    __LOG_PATH__ = config['DEFAULT']['LOG_PATH']

if "LOG_FILE" in os.environ:
    __LOG_FILE__ = os.getenv("LOG_FILE")
else:
    __LOG_FILE__ = config['DEFAULT']['LOG_FILE']
