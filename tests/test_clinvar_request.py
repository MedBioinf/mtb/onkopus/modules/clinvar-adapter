import unittest
import clinvar_adapter.data_parser

class TestClinvarRequest(unittest.TestCase):

    def test_clinvar_request(self):
        genompos=["chr7:140753336A>T","chr1:12975309TTCTC>T"]
        genome_version="hg38"

        response = clinvar_adapter.data_parser.get_clinvar_entry(genompos,genome_version)
        print(response)