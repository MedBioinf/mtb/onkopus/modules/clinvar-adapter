import unittest
import clinvar_adapter.data_parser

class TestTabixQueryGeneration(unittest.TestCase):

    def test_tabix_generation(self):
        variant_list=["chr7:140753336A>T","chr1:12975309TTCTC>T"]
        q= clinvar_adapter.data_parser.generate_tabix_query(variant_list)
        print(q)

