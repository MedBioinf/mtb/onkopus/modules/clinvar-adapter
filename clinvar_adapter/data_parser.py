import traceback, re, subprocess, logging
import conf.read_config as conf_reader


def generate_tabix_query(variant_list):
    """


    :param variant_list:
    :return:
    """
    response = {}
    genome_pos_exp = "(chr)([0-9|X|Y]+):(g\.)?([0-9]+)([A|G|C|T|N]+)>([A|G|C|T|N]+)"
    p = re.compile(genome_pos_exp)

    tabix_q = ''
    for variant in variant_list:
        response[variant] = {}

        try:
            groups = p.match(variant)
            chrom = groups.group(2)
            pos = groups.group(4)

            tabix_q += chrom + ":" + pos + "-" + pos + " "
        except:
            logger = logging.getLogger('root')
            msg = "Could not parse genomic location: ",variant+": "+traceback.format_exc().format()
            logger.debug(msg)

    return tabix_q


def get_clinvar_entry(variant_list, genome_version):
    """
    Retrieves the ClinVar entries for a list of genomic locations

    :param variant_list:
    :param genome_version:
    :return:
    """
    if genome_version=="hg38":
        db_file = conf_reader.__DATA_PATH__ + "/grch38" + "/" + conf_reader.__DATABASE_FILE_HG38__
    else:
        db_file = conf_reader.__DATA_PATH__ + "/grch37" + "/" + conf_reader.__DATABASE_FILE_HG19__

    tabix_q = generate_tabix_query(variant_list)

    tabix_query = "tabix " + db_file + " "+ tabix_q
    print(tabix_query)

    process = subprocess.Popen(tabix_query.split(), stdout=subprocess.PIPE)
    output, error = process.communicate()
    output = output.decode("utf-8")

    results = output.split('\n')
    response = {}
    for result in results:
        #print(result)
        elements = result.split("\t")
        if len(elements) > 1:
            chrom_res = elements[0]
            pos_res = elements[1]
            ref_res = elements[3]
            alt_res = elements[4]
            clinvar_id = elements[2]
            var = "chr" + chrom_res + ":" + pos_res + ref_res + ">" + alt_res
            if var in variant_list:
                if var not in response:
                    response[var] = {}
                response[var][conf_reader.config["DEFAULT"]["MODULE_PREFIX"]] = generate_json_obj_from_database_array(elements,clinvar_id)
            else:
                #print("variant not found: ",var,": ",variant_list)
                pass
    return response


def generate_json_obj_from_database_array(elements,clinvar_id):
    """
    Generates a JSON dictionary from a dbNSFP database entry array

    :param elements:
    :return:
    """
    json_obj = {}

    info_col = elements[7]
    features = elements[7].split(";")
    for feature in features:
        if len(feature.split("="))>1:
            key,val = feature.split("=")
            json_obj[key] = val
    json_obj["CLINVARID"] = clinvar_id

    return json_obj


