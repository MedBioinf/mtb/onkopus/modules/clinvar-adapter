FROM python:3.11.4-bookworm

RUN apt-get update
RUN apt-get upgrade -y

RUN apt-get install -y python3-pip python3-dev tabix less

# Create virtual environment
RUN python3 -m venv /opt/venv
RUN /opt/venv/bin/python3 -m pip install --upgrade pip

WORKDIR /scripts
COPY ./preprocess.sh /scripts/preprocess.sh

WORKDIR /app/
COPY ./requirements.txt /app/requirements.txt
RUN /opt/venv/bin/pip install -r requirements.txt

COPY . /app/
CMD ["export", "PYTHONPATH=/app"]

EXPOSE 8092

ENTRYPOINT [ "/scripts/preprocess.sh" ]
